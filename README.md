# README #

This README would normally document whatever steps are necessary to get your application up and running.



* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
Setup the environment (from the wacomm repository directory)

Edit and revise the etc/profile

source etc/profile

mkdir $HDF5_HOME

mkdir $NETCDF

mkdir $NETCDF_FORTRAN

Set the compiler options

- GNU:

export CC=gcc

export FC=gfortran

export F90=gfortran

export CXX=g++

- PGI:

export CFLAGS="-O2 -Msignextend -V"

export FFLAGS="-O2 -w -V"

export CPPFLAGS="-DNDEBUG -DpgiFortran"

* Dependencies
Enter in the distribution directory

cd dist

- HDF5 with zlib

Download the latest version of HDF5 source code from https://www.hdfgroup.org/HDF5/release/obtainsrc.html (we used 1.8.16, but it could be different)

wget http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.16.tar.gz

tar -xvzf hdf5-1.8.16.tar.gz

rm hdf5-1.8.16.tar.gz

cd hdf5-1.8.16

./configure --prefix=$HDF5_HOME --enable-hl --enable-shared --enable-fortran --enable-production --enable-unsupported --enable-cxx --with-zlib --with-szlib --enable-threadsafe --with-pthread

make

make install

- NetCDF4 (with all netcdf features)
Download the latest version of NETCDF4 C Library from http://www.unidata.ucar.edu/downloads/netcdf/ (we used 4.4.0, but it could be different)

wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4.4.0.tar.gz

tar -xvzf netcdf-4.4.0.tar.gz

rm netcdf-4.4.0.tar.gz

cd netcdf-4.4.0

export CPPFLAGS="-I$HDF5_HOME/include"

export LDFLAGS="-L$HDF5_HOME/lib"

./configure --prefix=$NETCDF --enable-netcdf-4 --enable-shared  --enable-dap --with-hdf5=$HDF5_HOME

make

make install

- NetCDF4 (fortran bindings)
Download the latest version of NETCDF4 Fortran bindings form from http://www.unidata.ucar.edu/downloads/netcdf (we used 4.4.2,  but it could be different)

wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-fortran-4.4.2.tar.gz

tar -xvzf netcdf-fortran-4.4.2

rm netcdf-fortran-4.4.2.tar.gz

cd netcdf-fortran-4.4.2

export CPPFLAGS="-I$NETCDF/include"

export LDFLAGS="-L$NETCDF/lib"

./configure --prefix=$NETCDF_FORTRAN

make

make install


* How to run tests
 INPUT
  - NetCDF file with current surface u,v components on Arakawa C-Grid (es. Regional Ocean Model System [ROMS] output)
  - namelist.wacomm file with sources positions in point grid and number of particles emitted by each source.
 COMPILE AND RUN WACOMM 
  - cd WACOMM_HOME
  - set number of execution core in wacomm.F90 file
  - edit namelist.wacomm with the information relative your run
  - make COMPILER=gnu
  - ./wacomm.exe namelist.wacomm  
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Raffaele Montella (raffaele.montella@uniparthenope.it)
* Angelo Riccio (angelo.riccio@uniparthenope.it)
* Diana Di Luccio (diana.diluccio@uniparthenope.it)
* Pasquale Troiano (pasquale.troniano@uniparhenope.it)

### Changes history ###
* 2016/01/16 First refactory and namelist update with omp controls - Raffaele Montella
* 2015/11/01 Testing on P8 Cluster - Diana di Luccio, Pasquale Troiano
* 2015/10/01 Testing on XEON Cluster - Diana di Luccio, Pasquale Troiano
* 2015/09/12 Shared memory parallelization design - Diana di Luccio, Pasquale Troiano
* 2014/10/10 Restart implementation - Angelo Riccio
* 2013/06/06 New pollutant sources parametrization - Orsa/IZSM Team
* 2013/05/05 Operational production interface implementation - Raffaele Montella
* 2012/10/06 Roms (https://www.myroms.org) interface - Angelo Riccio
* 2012/09/09 Heavy code refacoring - Angelo Riccio
* 2012/06/06 Pollutant sources definition - Orsa Team
* 2012/05/05 Operational production interface design -  Raffaele Montella
* 2006/01/01 New sr for random numbers by alain noullez - Andrea Doglioli
* 2005/12/05 Estethic restyling - Andrea Doglioli
* 2004/12/05 Sintetic currentmeters in tig2 position - Andrea Doglioli
* 2004/06/05 New bcond for wekman based on messo zero-gradient - Andrea Doglioli
* 2004/06/01 Biodegradation findlay-watling model - A. Doglioli, P. Vassallo
* 2002/02/03 Offline model (v2.9) coupled with pom98 (http://www.ccpo.odu.edu/POMWEB/) - Marcello Megaldi - MSC Thesis
* 2000/02/02 Lagrangian assessment for marine pollution 3-dimensional model - Offlile model (v1.9) coupled with Mike21 (https://www.mikepoweredbydhi.com/products/mike-21) - Andrea Doglioli - MSC Thesis
